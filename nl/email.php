<?php
// $name = 'Jelmer van der Linde';
// $function = 'student';
// $study = 'Artificial Intelligence';
// $email = 'jelmer@ikhoefgeen.nl';
// $remarks = "Dit is een stukje\nonzin";
// $attending_day_1 = 'attending-day-1';
// $attending_day_2_program = 'day-2-program';
// $attending_day_2_dinner = 'day-2-dinner';
// $attending_day_2_party = 'day-2-party';

$attending_day_2 = $attending_day_2_program || $attending_day_2_dinner || $attending_day_2_party;

// Mapping for function to something readable
$function_mapping = array(
	'student' => 'student ' . $study,
	'alumnus' => 'alumnus ' . $study,
	'employee' => 'werknemer',
	'former-employee' => 'oud-werknemer',
	'other' => ''
);

// The ending greeting, fitting to the event the attendee is attending.
$greeting = array();

if ($attending_day_1)
	$greeting[] = 'symposium';

if ($attending_day_2)
	$greeting[] = 'lustrum';

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Bevestiging symposium/lustrum KI</title>
	<style>
	body {
		font: 13px/16px sans-serif;
	}
	</style>
</head>
<body>
<p>Beste <em><?=escape($name)?></em>,</p>

<p>Bij deze bevestigen wij dat we je inschrijving hebben ontvangen. Je hebt je ingeschreven voor de volgende onderdelen:</p>

<ul>
	<?php if ($attending_day_1): ?><li>Dag 1 &mdash; Symposium</li><?php endif ?>
	<?php if ($attending_day_2): ?><li>Dag 2 &mdash; Lustrum</li><ul><?php endif ?>
		<?php if ($attending_day_2_program): ?><li>Dagprogramma</li><?php endif ?>
		<?php if ($attending_day_2_dinner): ?><li>Dinner</li><?php endif ?>
		<?php if ($attending_day_2_party): ?><li>Feest</li><?php endif ?>
	<?php if ($attending_day_2): ?></ul><?php endif ?>
</ul>

<p>
<?php if ($function_mapping[$function]): ?>Tevens heb je aangegeven dat je een <em><?=escape($function_mapping[$function])?></em> bent.<?php endif ?>
<?php if (strlen(trim($remarks))): ?>De volgende verdere opmerkingen hebben wij ontvangen:<?php endif ?>
</p>
<?php if (strlen(trim($remarks))): ?>
<blockquote>
<?= nl2br(escape($remarks)) ?>
</blockquote>
<?php endif ?>

<p>Is één of meer van deze gegevens onjuist, laat het ons dan zo spoedig mogelijk weten door een mail te sturen naar <a href="mailto:info@thecontinuum.nl">info@thecontinuum.nl</a>.</p>
<p><strong>Mocht je verhinderd zijn te komen, laat dit ook weten door een mail te sturen.</strong></p>

<p>Tot op het <?=implode(' en ', $greeting)?>!</p>

<p>De SympoCie en het Lustrumcomité</p>
</body>
</html>