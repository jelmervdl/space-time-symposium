<form method="post" action="#sign-up" class="sign-up-form">
	<input type="hidden" name="form-id" value="sign-up-form">

	<?php if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['form-id'] == 'sign-up-form' && handle_sign_up_form()): ?>
		<p class="happy-message">U bent ingeschreven en zal binnen enkele minuten een bevestigingsemail ontvangen!</p>
	<?php else: ?>
		<p>Schrijf je hier in voor (een van) beide dagen. In verband met de organisatie willen we iedereen vragen zich tijdig aan te melden. Afzeggen kan door te mailen naar: <a href="mailto:info@thecontinuum.nl">info@thecontinuum.nl</a>.</p>
		<p>Deelname is gratis.</p>
	<?php endif ?>

	<?php begin_form_row('name') ?>
		<label for="sign-up-name">Naam: <span class="error-message">Vul alstublieft uw naam in</span></label>
		<?php form_field('text', 'name', array('placeholder' => 'voor- en achternaam', 'required')) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('function') ?>
		<label for="sign-up-function">Ik ben een… <span class="error-message">Vul alstublieft uw relatie met het evenement in</span></label>
		<?php form_select('function', array('required'), array(
			array('Kies uw relatie tot het evenement', array('disabled')),
			'student' => array('Student'),
			'alumnus' => array('Alumnus'),
			'employee' => array('Medewerker'),
			'former-employee' => array('Oud-medewerker'),
			'other' => array('Anders'))) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('study') ?>
		<label for="sign-up-study">Studie:</label>
		<?php form_field('text', 'study', array('placeholder' => 'bijv. Kunstmatige Intelligentie of Informatica')) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('email') ?>
		<label for="sign-up-email">E-mail: <span class="error-message">Vul alstublieft uw e-mailadres in</span></label>
		<?php form_field('email', 'email', array('required')) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('attending') ?>
		<label>Ik wil me opgeven voor: <span class="error-message">Kies de dagdelen die u van plan bent bij te wonen</span></label>
		<ul>
			<li>
					<li><label><?php form_checkbox('day-2-program') ?>Dagprogramma</label></li>
					<li><label><?php form_checkbox('day-2-dinner') ?>Diner</label></li>
					<li><label><?php form_checkbox('day-2-party') ?>Feest</label></li>

			</li>
		</ul>
	<?php end_form_row() ?>

	<?php begin_form_row('remarks') ?>
		<label for="sign-up-remarks">Opmerkingen:</label>
		<?php form_textarea('remarks', array('placeholder' => 'bijv. heeft u allergieën…')) ?>
	<?php end_form_row() ?>

	<div class="controls">
		<button type="submit">Schijf mij in</button>
	</div>
</form>