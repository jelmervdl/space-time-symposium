<!DOCTYPE html>
<html>
  <head>
    <title>Space/Time</title>
    <?php include '../include/header.php' ?>
  </head>
  <body id="landing-page">
    <div class="container">
      <div class="content">
        <h1 id="the-continuum">The Continuum</h1>
        
        <section id="space" class="panel">
          <h1>Space</h1>
          <time>12 december 2013</time>
          <p>Symposium<br>Studievereniging Cover</p>
          <a href="space" class="read-on-button">Continue</a>
        </section>
        
        <section id="time" class="panel">
          <h1>Time</h1>
          <time>13 december 2013</time>
          <p>Lustrum<br>Kunstmatige Intelligentie Groningen</p>
          <a href="time" class="read-on-button">Continue</a>
        </section>
      </div>
    </div>
    <a href="../en" class="switch-to-english">English</a>
  </body>
</html>