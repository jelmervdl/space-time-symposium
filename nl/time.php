<!DOCTYPE html>
<html>
  <head>
    <title>Time</title>
    <?php include '../include/header.php' ?>
  </head>
  <body id="time-page">
    <header class="parallax">
      <h1>Time</h1>
    </header>
    
    <nav>
      <ul>
        <li><a href="#intro">Lustrum</a></li>
        <li><a href="#program">Programma</a></li>
        <li><a href="#competition">Prijsvraag</a></li>
        <li><a href="#location">Locatie</a></li>
        <li><a href="#sign-up">Inschrijven</a></li>
        <li><a href="#sponsors">Sponsors</a></li>
        <li><a href="space" class="link-to-space">&amp; Space</a></li>
        <li><a href="../en/time" class="switch-to-english">English</a></li>
      </ul>
    </nav>

    <section id="intro">
      <h2>Lustrum</h2>
      <article>
        <h3>20 jaar KI</h3>
        <p>In september 1993 opende de opleiding Technische Cognitiewetenschap te Groningen voor de eerste studenten haar deuren. Twintig jaar verder is er veel veranderd. We heten inmiddels Kunstmatige Intelligentie, organiseren één Bachelor- en twee Masteropleidingen en hebben in totaal meer dan 250 studenten.</p>
        <p>Dit 20-jarig jubileum willen we graag vieren met iedereen die betrokken is of was bij de opleiding KI in Groningen, en met alle anderszins geïnteresseerden. Het is dus tijd voor een feestelijke dag!</p>
        <p>Op vrijdag 13 december zal er daarom een dag plaatsvinden die geheel in het thema van ‘Tijd’ staat. Overdag zullen enkele sprekers vertellen over KI-onderzoek dat te maken heeft met tijd, en zullen het verleden, heden en toekomst van de opleiding KI in Groningen belicht worden. Ook hebben we meerdere leuke activiteiten op het programma staan waarin studenten, staf, alumni en oud–medewerkers op een informele manier de opleiding KI in al haar facetten kunnen meemaken.</p>
        <p>’s Avonds hebben we een gezamenlijk diner, waarna we afsluiten met een feest in de Puddingfabriek.</p>
        <p>Deze dag staat echter niet op zichzelf. Deze lustrumdag is onderdeel van een tweedaags evenement, ‘The Continuum’, georganiseerd in samenwerking met studievereniging Cover. Op 12 december zal Cover daarom <a href="space">een groots symposium met als thema ‘Space’</a> organiseren.</p>
        <p>We willen een ieder van harte uitnodigen voor beide dagen. <a href="#sign-up">Inschrijven kan hier</a>. In verband met de organisatie willen we iedereen vragen zich zo spoedig mogelijk aan te melden. Wil je in contact komen met de organisatie van deze lustrumdag, mail dan met: lustrumki@gmail.com</p>
	 <p>Namens het lustrumcomité: Tjeerd Andringa, Elina Sietsema, Inge Slingerland </p>
      </article>
    </section>

    <section id="program">
      <h2>Programma</h2>
      <article>
        <h3>Dagprogramma</h3>
        <p>Het dagprogramma zal plaatsvinden in het <a href="#location">Grand Theatre</a>. Schrijf je van tevoren in!</p>
        <table>
          <thead>
            <tr>
              <th>Tijd</th>
              <th>Activiteit</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>09:15</td>
              <td>Binnenkomst</td>
            </tr>
            <tr>
              <td>10:00</td>
              <td>Opening door Lambert Schomaker en Sietse van Netten</td>
            </tr>
 	     <tr>
              <td>10:15</td>
              <td><em>20 jaar KI in Groningen</em>, door Frans Zwarts, Tjeerd Andringa, Niels Taatgen en Petra Hendriks</td>
            </tr>
 	     <tr>
              <td>11:00</td>
              <td>Koffiepauze</td>
            </tr>
	     <tr>
              <td>11:15</td>
              <td>Debat tussen studenten en staf</td>
            </tr>
	     <tr>
              <td>12:00</td>
              <td><em>KI en de toekomst</em>, door Nanda Wijermans (Stockholm Resilience Centre)</td>
            </tr>
	      <tr>
              <td>12:45</td>
              <td>Lunch</td>
            </tr>
	      <tr>
              <td>13:30</td>
              <td><em>ALICE</em>, door Niels Taatgen</td>
            </tr>
		<tr>
		<tr>
              <td>14:00</td>
              <td>Pubquiz<td>
            </tr>
		<tr>

              <td>15:15</td>
              <td>Koffiepauze</td>
            </tr>
	      <tr>
              <td>15:30</td>
              <td>Praatje door Bas Haring (<a href="http://www.basharing.com">www.basharing.com</a>)</td>
            </tr>
	      <tr>
              <td>16:30</td>
              <td><a href="#competition">Prijsuitreiking</a></td>
            </tr>
          </tbody>
        </table>
      </article>
      <article>
        <h3>Avondprogramma</h3>
        <p>Kun je er overdag niet bij zijn, maar wil je wel graag aanwezig zijn bij het diner en/of feest? Dat kan! </p>
	<p>'s Avonds zal het programma in <a href="#location">de Puddingfabriek te Groningen</a> plaatsvinden, waar we welkom zijn vanaf 17.00 uur.</p>
 	<p>Vanaf 18.30 is er een gratis buffet, daarna zal het feest starten.</p>
      </article>
    </section>

    <section id="competition">
      <h2>Prijsvraag</h2>
      <article>
          <h3>Prijsvraag: Hoe kan KI helpen in rampgebieden?</h3>
        
<p>Stel je een derdewereld land voor na een grote ramp zoals een tornado, aardbeving of tsunami. De bevolking heeft geen water, electriciteit of voedsel en het is moeilijk voor doctoren en andere hulp om het getroffen gebied binnen te komen.</p>
<p><em>Heb jij een idee, gebaseerd in de Kunstmatige Intelligentie, dat in een dergelijke situatie van dienst kan zijn?</em> Je mag een oplossing verzinnen voor een subonderdeel van het probleem dat jij interessant vindt: slachtoffers localiseren onder het puin, routes om het gebied binnen te komen vinden terwijl alle infrastructuur kapot is, een manier vinden om hulpmiddelen goed te verdelen over alle overlevenden, slachtoffers identificeren, etc.</p>
<h4>Details</h4>
<p>Je mag zowel alleen als in kleine groepjes tot maximaal 3 studenten meedoen. Je kunt je idee op verschillende manieren inleveren: bijvoorbeeld door een video te maken waarin je je idee presenteert, een kort essay erover schrijven, een poster maken, of een of ander prototype. Hoe je het ook doet, maak duidelijk waarom jouw idee relevant is voor KI en hoe het helpt met betrekking tot rampgebieden.</p>
<p>Daarnaast willen we elk groepjes ook vragen om antwoord te geven, in tekst, op de volgende vragen:</p>
<ul>
	<li>Voor welk probleem is jullie idee een oplossing, en waarom?</li>
	<li>Noem andere applicaties waarbij jullie idee ook van nut kan zijn.</li>
	<li>Hoe maakt jullie idee gebruik van Kunstmatige Intelligentie?</li>
</ul>
<p></p>
<p>De 3 beste inzendingen zullen door een jury gekozen worden, en mogen een korte presentatie van maximaal 5 minuten tijdens de lustrumdag geven. De winnende groep zal ter plekke gekozen worden en wint 100 euro prijsgeld. De tweede prijs is 50 euro. </p>
<p>De deadline voor het insturen van je inzending is woensdag 11 december. Deze kun je mailen naar <a href="mailto:lustrumki@gmail.com">lustrumki@gmail.com</a>. Als je nog vragen hebt kun je die natuurlijk ook mailen.</p>
      <p><b>De deadline is uitgesteld tot woensdag 11 december, 23.59!</b></p>
      </article>
    </section>

    <section id="location">
      <h2>Locatie</h2>
      <article class="location" data-lat="53.218419" data-lng="6.568711">
        <h3>Grand Theatre</h3>
        <p><a href="http://www.grandtheatregroningen.nl/kassacontact/routebeschrijving">Route naar Grand Theatre</a></p>
      </article>
      <article class="location" data-lat="53.210356" data-lng="6.570327">
        <h3>Puddingfabriek</h3>
        <p><a href="http://www.depudding.nl/routebeschrijving/">Route naar Puddingfabriek</a></p>
      </article>
    </section>

    <section id="sign-up">
      <h2>Inschrijven</h2>
      <article>
        <?php include '../include/sign-up.php' ?>
      </article>
    </section>


    <section id="sponsors">
      <h2>Sponsors</h2>
      <article id="quintor">
        <p><img src="../img/quintor.png" alt="quintor" /></p>
      </article>
    </section>

    <footer>
      <article>
        <p>Vragen? Opmerkingen? Stuur een e-mail naar <a href="mailto:lustrumki@gmail.com">lustrumki@gmail.com</a></p>
      </article>
    </footer>

    <?php include '../include/footer.php' ?>
  </body>
</html>