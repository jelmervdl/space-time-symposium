<?php

if (preg_match('/(^|,)nl(,|;|$)/', $_SERVER['HTTP_ACCEPT_LANGUAGE']))
	header('Location: nl/');
else
	header('Location: en/');