<!DOCTYPE html>
<html>
  <head>
    <title>Space/Time</title>
    <?php include '../include/header.php' ?>
  </head>
  <body id="landing-page">
    <div class="container">
      <div class="content">
        <h1 id="the-continuum">The Continuum</h1>
        
        <section id="space" class="panel">
          <h1>Space</h1>
          <time>12 December 2013</time>
          <p>Symposium<br>Study Association Cover</p>
          <a href="space" class="read-on-button">Continue</a>
        </section>
        
        <section id="time" class="panel">
          <h1>Time</h1>
          <time>13 December 2013</time>
          <p>Lustrum<br>Artificial Intelligence Groningen</p>
          <a href="time" class="read-on-button">Continue</a>
        </section>
      </div>
    </div>
    <a href="../nl" class="switch-to-dutch">Nederlands</a>
  </body>
</html>