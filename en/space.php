<!DOCTYPE html>
<html>
  <head>
    <title>Space</title>
    <?php include '../include/header.php' ?>
  </head>
  <body id="space-page">
    <header class="parallax">
      <h1>Space</h1>
    </header>
    
    <nav>
      <ul>
        <li><a href="#intro">Symposium</a></li>
    <!--    <li><a href="#program">Program</a></li>
        <li><a href="#speakers">Speakers</a></li>
        <li><a href="#location">Location</a></li>
        <li><a href="#organisation">Organisation</a></li>
        <li><a href="#sign-up">Sign up</a></li> -->
        <li><a href="#sponsors">Sponsors</a></li>
        <li><a href="time" class="link-to-time">&amp; Time</a></li>
        <li><a href="../nl/space" class="switch-to-dutch">Nederlands</a></li>
      </ul>
    </nav>

    <section id="intro">
      <h2>Symposium</h2>
      <article>
        <p>Due to medical circumstances one of our speakers had to cancel. We are therefore, regrettably, forced to cancel the symposium this Thursday, as we did not find a symposium with only one speaker worthwhile. We hope for your understanding.
<br /><br />
Kind regards,<br />
The SympoCie</p>
      </article>
    </section>
<!--
    <section id="program">
      <h2>Program</h2>
      <article>
        <p>The programme will be announced soon.</p>
        <table>
          <thead>
            <tr>
              <th>When</th>
              <th>What</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>12:15</td>
              <td>Entrance</td>
            </tr>
            <tr>
              <td>12:45</td>
              <td><em>Opening of the Symposium</em></td>
            </tr>
            <tr>
              <td>13:00</td>
              <td><em>The End of the Universe's Darkness</em> by <a href="#speaker-zaroubi">Saleem Zaroubi</a></td>
            </tr>
            <tr>
              <td>14:15</td>
              <td><em>Coffee break</em></td>
            </tr>			
            <tr>
              <td>14:45</td>
              <td><em>Space-based Radio Astronomy Array for Ultra-low frequency observations</em> by <a href="#speaker-rajan">Raj Rajan</a></td>
            </tr>
            <tr>
              <td>16:00</td>
              <td><em>Drinks</em></td>
            </tr>
          </tbody>
        </table>
      </article>
    </section>

    <section id="speakers">
      <h2>Speakers</h2>
 <article id="speaker-zaroubi">
        <p><h2>Saleem Zaroubi</h2><br /><img src="../img/space/speakers/Saleem.jpg" alt="Saleem Zaroubi" class="speaker" />Saleem is a professor at the Kapteyn Astronomical Institute.
He is originally a Palestinian-Israeli who was born in Nazareth, Israel. Studied in the Technion, Haifa for his BA and MA
and finished his PhD in the Hebrew University of Jerusalem in 1994, specializing in Cosmology. He spend a few years
at the University of California at Berkeley and 5 years at Max Planck Institute for Astrophysics. In 2004 Saleem accepted an 
offer from the University of Groningen and has been here since then. His main field of research is Astrophysical Cosmology
and physics of the intergalactic medium, especially during the early Universe. He is one of the leaders of the LOFAR Epoch
of Reionization key science project which attempts to detect the radiation coming from neutral hydrogen during the epoch
in which the first galaxies and stars were formed. </p>
		<p><b>The End of the Universe's Darkness <br /></b><em>
          A tale of a mighty telescope, powerful computers and a feeble signal.</em><br /><br />
The Universe started its first galaxies around a few hundred million years after the Big Bang.
These galaxies mark a major phase transition in the Universe in which its gas transforms from
neutral to ionized. This phase, know as the Epoch of Reionization, is one of the least studied
periods in cosmology. The Low Frequency Array (LOFAR)  radio telescope will track neutral
hydrogen during this epoch for the first time, hence opening a completely new window to the
Universe.

The signal coming from the dawn of Universe is very feeble and is hidden under many layers of
contamination, distortion and noise. This very challenging project requires the extraction of the
signal by sifting through petabytes of data using sophisticated computational techniques and
very powerful computers.
      </article>

<article id="speaker-rajan">
        <p><h2>Raj Thilak Rajan</h2><br /><img src="../img/space/speakers/rajan.jpg" alt="Raj Rajan" class="speaker" />Raj Thilak Rajan received M.Sc (with distinction) and B.Sc (with distinction) in Electronic science from University of Pune, India in 2006 and 2004 respectively. He is presently with the Digital and Embedded Signal Processing (DESP) RnD group at ASTRON, Netherlands center for radio astronomy, The Netherlands. He is also a PhD candidate at TU Delft, The Netherlands, where he works on signal processing aspects of the OLFAR (Orbiting Low Frequency Antenna array for Radio astronomy) project under the supervision of Prof. dr. ir. Alle-Jan van der Veen. Previously, he worked at INFN-Politecnio di Bari (Italy) and was a visiting researcher at CERN (Switzerland) for the Large Hadron Collider project in 2007-2008.
 </p>
		<p><b>Space-based Radio Astronomy Array for Ultra-low frequency observations <br /></b><em>
          Feasibility and technological challenges.</em><br /><br />
The success of terrestrial radio astronomy is credited to the transparency of the ionosphere in the frequency spectrum 30MHz - 3 GHz, in addition to technological advances in the past few decades. However, due to ionospheric scintillation below 30 MHz and opaqueness below 10 MHz, earth-bound radio astronomy observations at these wavelengths are severely limited in sensitivity and spatial resolution, or entirely impossible.  An unequivocal solution is to have a radio telescope in space to explore the frequency band 0.3MHz - 30MHz.  The envisioned space based radio telescope will consist of 10 or more scalable and autonomous nano satellites, synthesizing an interferometric array observing at these very long wavelengths. In addition, the satellite cluster will be a self-reliant co-operative network employing distributed communication, processing, and navigation techniques, which present a diverse portfolio of challenges.
      </article>
    </section>

    <section id="location">
      <h2>Location</h2>
      <article class="location" data-lat="53.218419" data-lng="6.568711">
        <h3>Grand Theatre</h3>
        <p><a href="http://www.grandtheatregroningen.nl/kassacontact/routebeschrijving">Route naar Grand Theatre</a></p>
      </article>
      <!--
      <article class="location" data-lat="53.210356" data-lng="6.570327">
        <h3>Puddingfabriek</h3>
        <p><a href="http://www.depudding.nl/routebeschrijving/">Route naar Puddingfabriek</a></p>
      </article>
      -->
    </section>

    <section id="organisation">
      <h2>Organisation</h2>
      <article>
        <p>The symposium, which will take place on the first day, is organized by the symposium committee of <a href="http://www.svcover.nl/">Cover</a>. The committee consists of five enthousiastic students who study artificial intelligence and computer science. These students are Marten Schutten (Chairman, 5th year AI), Diederik Eilers (Secretary, 2nd year AI), Daan Sijbring (Treasurer, 2nd year AI), Sophie Hugenholtz (Vice-chairman, 2nd year CS) and Jordi van Giezen (General member, 4th year AI).</p>
        <p>We have been working hard for a few months on organizing this day. Every year, a symposium takes place, but due to the collaboration with the staff of artificial intelligence, this year's symposium is somewhat different. After working closely with the staff, we organized a two-day event which will not only entertain and educate students, but is also accessible and fun for staff members, alumni and employees.</p>
        <p><img src="../img/space/sympocie.jpg" widht="700" height="391" alt="sympocie"></p>
	</article>
    </section>

    <section id="sign-up">
      <h2>Sign up</h2>
      <article>
        <?php include '../include/sign-up.php' ?>
      </article>
    </section> -->

    <section id="sponsors">
      <h2>Sponsors</h2>
      <article id="quintor">
        <p><img src="../img/quintor.png" alt="quintor" /></p>
      </article>
    </section>

    <footer>
      <article>
        <p>Questions? Remarks? Send an email to sympocie@svcover.nl</p>
      </article>
    </footer>

    <?php include '../include/footer.php' ?>
  </body>
</html>