<?php
$attending_day_2 = $attending_day_2_program || $attending_day_2_dinner || $attending_day_2_party;

// Mapping for function to something readable
$function_mapping = array(
	'student' => 'a student ' . $study,
	'alumnus' => 'an alumnus ' . $study,
	'employee' => 'an employee',
	'former-employee' => 'a former employee',
	'other' => ''
);

// The ending greeting, fitting to the event the attendee is attending.
$greeting = array();

if ($attending_day_1)
	$greeting[] = 'the symposium';

if ($attending_day_2)
	$greeting[] = 'the lustrum';

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Confirmation symposium/lustrum AI</title>
	<style>
	body {
		font: 13px/16px sans-serif;
	}
	</style>
</head>
<body>
<p>Dear <em><?=escape($name)?></em>,</p>

<p>With this e-mail we confirm that we have recieved your registration. You registered for the following activities:</p>

<ul>
	<?php if ($attending_day_1): ?><li>Day 1 &mdash; Symposium</li><?php endif ?>
	<?php if ($attending_day_2): ?><li>Day 2 &mdash; Lustrum</li><ul><?php endif ?>
		<?php if ($attending_day_2_program): ?><li>Day program</li><?php endif ?>
		<?php if ($attending_day_2_dinner): ?><li>Dinner</li><?php endif ?>
		<?php if ($attending_day_2_party): ?><li>Party</li><?php endif ?>
	<?php if ($attending_day_2): ?></ul><?php endif ?>
</ul>

<p>
<?php if ($function_mapping[$function]): ?>You mentioned you are <em><?=escape($function_mapping[$function])?></em>.<?php endif ?>
<?php if (strlen(trim($remarks))): ?>We also received the following remarks:<?php endif ?>
</p>
<?php if (strlen(trim($remarks))): ?>
<blockquote>
<?= nl2br(escape($remarks)) ?>
</blockquote>
<?php endif ?>

<p>If this information is incorrect, please let us know by sending an e-mail to <a href="mailto:lustrumki@gmail.com">lustrumki@gmail.com</a>.</p>
<p><strong>If you won't be able to come, please let us know as well by sending in e-mail.</strong></p>

<p>We look forward to seeing you at <?=implode(' and ', $greeting)?>!</p>

<p>The SympoCie and the Lustrum committee</p>
</body>
</html>