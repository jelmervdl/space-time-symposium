<!DOCTYPE html>
<html>
  <head>
    <title>Time</title>
    <?php include '../include/header.php' ?>
  </head>
  <body id="time-page">
    <header class="parallax">
      <h1>Time</h1>
    </header>
    
    <nav>
      <ul>
        <li><a href="#intro">Lustrum</a></li>
        <li><a href="#program">Program</a></li>
        <li><a href="#competition">Competition</a></li>
        <li><a href="#location">Location</a></li>
        <li><a href="#sign-up">Sign up</a></li>
        <li><a href="#sponsors">Sponsors</a></li>
        <li><a href="space" class="link-to-space">&amp; Space</a></li>
        <li><a href="../nl/time" class="switch-to-dutch">Nederlands</a></li>
      </ul>
    </nav>

    <section id="intro">
      <h2>Lustrum</h2>
      <article>
        <h3>20th anniversary of Artificial Intelligence in Groningen</h3>
        <p>In September 1993 ‘Technische Cognitiewetenschap’, a newcomer in the University of Groningen, opened her doors for the first students. Twenty years later a lot has changed. We are now called Artificial Intelligence, we organize one Bachelor’s and two Master’s programmes and have in total more than 250 students.</p>
        <p>We want to celebrate this 20-year anniversary with everybody who was or is involved in any way with Artificial Intelligence in Groningen. It’s time for a day filled with festivities!</p>
        <p>On Friday December 13th we will organise a so-called 'lustrum' in the theme of ‘Time’. During the day some speakers will talk about time related AI-research. The past, present and future of AI in Groningen will be highlighted.  There will be plenty of activities where students, staff, alumni and former staff can experience Artificial Intelligence in an informal way.</p>
        <p>In the evening you can join us for dinner, concluded with a party in the ‘Puddingfabriek’.</p>
        <p>This day does not stand on its own. This anniversary is part of a two-day event called ‘The Continuum’, organized together with study association Cover. This is a <a href="space">symposium</a> organized by Cover on December 12th on the theme of ‘Space’.</p>
        <p>Because of organizational reasons we would like to ask you to <a href="#sign-up">sign up</a> as early as possible. If you want to contact the organization of the AI-anniversary, you can send an e-mail to lustrumki@gmail.com </p>
        <p>On behalf of the lustrumcommittee: Tjeerd Andringa, Elina Sietsema, Inge Slingerland </p>
      </article>
    </section>

    <section id="program">
      <h2>Program</h2>
      <article>
        <h3>Day program</h3>
        <p>The day program will take place in the <a href="#location">Grand Theatre</a> in Groningen. Please register!</p>
        <table>
          <thead>
            <tr>
              <th>Time</th>
              <th>Activity</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>09:15</td>
              <td>Entry</td>
            </tr>
            <tr>
              <td>10:00</td>
              <td><em>Opening</em> by Lambert Schomaker and Sietse van Netten</td>
            </tr>
 	     <tr>
              <td>10:15</td>
              <td><em>20 years AI in Groningen</em>, by Frans Zwarts, Tjeerd Andringa, Niels Taatgen and Petra Hendriks</td>
            </tr>
 	     <tr>
              <td>11:00</td>
              <td>Coffee break</td>
            </tr>
	     <tr>
              <td>11:15</td>
              <td>Debate between students and staf</td>
            </tr>
	     <tr>
              <td>12:00</td>
              <td><em>AI and the future</em>, by Nanda Wijermans (Stockholm Resilience Centre)</td>
            </tr>
	      <tr>
              <td>12:45</td>
              <td>Lunch</td>
            </tr>
	      <tr>
              <td>13:30</td>
              <td><em>ALICE</em>, by Niels Taatgen  </td>
            </tr>
		<tr>
              <td>14:00</td>
              <td>Pubquiz</td>
            </tr>

		<tr>
              <td>15:15</td>
              <td>Coffee break</td>
            </tr>
	      <tr>
              <td>15:30</td>
              <td>Talk by Bas Haring (<a href="http://www.basharing.com">www.basharing.com</a>)</td>
            </tr>
	      <tr>
              <td>16:30</td>
              <td><a href="#competition">Award ceremony</a></td>
            </tr>
          </tbody>
        </table>
      </article>
      <article>
        <h3>Evening Program</h3>
        <p>Are you not available during the day, but do you want to celebrate the 20th anniversary of AI? You are very welcome to join us for the evening activities in <a href="#location">the Puddingfabriek te Groningen</a>, where we are welcome from 5pm onwards.</p>
	<p> At 6.30 pm there will be a free buffet, afterwards the party will start.</p>
       </article>
    </section>

    <section id="competition">
      <h2>Competition</h2>
      <article>
       <h3>Student contest: How can AI help in disaster areas?</h3>
        
<p>Imagine the aftermath of a big disaster like a tornado, earthquake or tsunami in a third world country. The locals have no water, power or food and it is hard for doctors and other help to access the affected area.</p>
<p><em>Can you think of a new idea, based in AI, to help in such a scenario?</em> You may focus on whatever sub-area of this problem you find interesting: localizing survivors under debris, finding routes to get to an area where all the infrastructure is impaired, distributing resources evenly over all victims, identifying victims, etc.</p>

<h4>Details</h4>
<p>You may participate alone or in small groups up to three members, and you can hand in your idea however you like: you can make a video to present your idea, write a short paper on it, make a poster, or make some kind of prototype. Make sure that it is clear why your idea is relevant to AI and how it helps in the given situation.</p>
<p>Additionally, we want to ask every group to give answers in text to the following questions:</p>
<ul>
	<li>What problem does your idea try to solve, and how?</li>
	<li>Name some other applications that your idea might be useful for.</li>
	<li>How does your idea use Artificial Intelligence?</li>
</ul>
<p></p>
<p>The 3 best entries will be chosen by a jury, and get to give a short presentation of maximum 5 minutes during the lustrum, the 13th of December. The winning group will be chosen on the day itself and will receive 100 euros worth of prize money. The second prize will be 50 euros.</p>
<p>The deadline for handing in your submission is on Wednesday the 11th of December. To hand in your submission, or if you have any questions, you can send an e-mail to: <a href="mailto:lustrumki@gmail.com">lustrumki@gmail.com</a>.</p>
<p><b>The deadline is postponed until Wednesday the 11th of December, 23.59!</b></p>      
</article>
    </section>

    <section id="location">
      <h2>Location</h2>
      <article class="location" data-lat="53.218419" data-lng="6.568711">
        <h3>Grand Theatre</h3>
        <p><a href="http://www.grandtheatregroningen.nl/kassacontact/routebeschrijving">Route naar Grand Theatre</a></p>
      </article>
      <article class="location" data-lat="53.210356" data-lng="6.570327">
        <h3>Puddingfabriek</h3>
        <p><a href="http://www.depudding.nl/routebeschrijving/">Route naar Puddingfabriek</a></p>
      </article>
    </section>

    <section id="sign-up">
      <h2>Sign up</h2>
      <article>
        <?php include '../include/sign-up.php' ?>
      </article>
    </section>

    <section id="sponsors">
      <h2>Sponsors</h2>
      <article id="quintor">
        <p><img src="../img/quintor.png" alt="quintor" /></p>
      </article>
    </section>

    <footer>
      <article>
        <p>Questions? Remarks? Send an email to lustrumki@gmail.com</p>
      </article>
    </footer>

    <?php include '../include/footer.php' ?>
  </body>
</html>