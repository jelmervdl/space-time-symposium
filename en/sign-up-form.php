<form method="post" action="#sign-up" class="sign-up-form">
	<input type="hidden" name="form-id" value="sign-up-form">

	<?php if ($_SERVER['REQUEST_METHOD'] == 'POST' && $_POST['form-id'] == 'sign-up-form' && handle_sign_up_form()): ?>
		<p class="happy-message">You have been signed up and should receive a confirmation email shortly!</p>
	<?php else: ?>
		<p>Sign up here for one or both days. Because of organizational reasons we would like to ask you to sign up timely. If you won’t be able to come, please let us know by sending us an e-mail to: <a href="mailto:info@thecontinuum.nl">info@thecontinuum.nl</a>.</p>
		<p>Participation is free.</p>
	<?php endif ?>

	<?php begin_form_row('name') ?>
		<label for="sign-up-name">Name: <span class="error-message">Please fill in your name</span></label>
		<?php form_field('text', 'name', array('placeholder' => 'first and last name', 'required')) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('function') ?>
		<label for="sign-up-function">I am a… <span class="error-message">Please specify your relation to the event</span></label>
		<?php form_select('function', array('required'), array(
			array('Please choose your relation to the event', array('disabled')),
			'student' => array('Student'),
			'alumnus' => array('Alumnus'),
			'employee' => array('Employee'),
			'former-employee' => array('Former Employee'),
			'other' => array('Other'))) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('study') ?>
		<label for="sign-up-study">Study:</label>
		<?php form_field('text', 'study', array('placeholder' => 'e.g. Artificial Intelligence or Computing Science')) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('email') ?>
		<label for="sign-up-email">Email: <span class="error-message">Please fill in your email address</span></label>
		<?php form_field('email', 'email', array('required')) ?>
	<?php end_form_row() ?>

	<?php begin_form_row('attending') ?>
		<label>I will be attending: <span class="error-message">Please select which moments you plan on attending</span></label>
		<ul>
			<li>
					<li><label><?php form_checkbox('day-2-program') ?>Day program</label></li>
					<li><label><?php form_checkbox('day-2-dinner') ?>Dinner</label></li>
					<li><label><?php form_checkbox('day-2-party') ?>Party</label></li>

			</li>
		</ul>
	<?php end_form_row() ?>

	<?php begin_form_row('remarks') ?>
		<label for="sign-up-remarks">Remarks:</label>
		<?php form_textarea('remarks', array('placeholder' => 'e.g. do you have allergies…')) ?>
	<?php end_form_row() ?>

	<div class="controls">
		<button type="submit">Sign up</button>
	</div>
</form>