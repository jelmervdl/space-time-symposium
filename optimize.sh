#!/bin/sh
shopt -s nullglob
for image in img/{space,time}/*.{jpg,png}; do
  convert "${image}" -resize 1600 "${image}"
done

/Applications/ImageOptim.app/Contents/MacOS/ImageOptim 2>/dev/null img/{space,time}/*.{jpg,png};
