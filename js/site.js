var sections = jQuery('header, section');
var nav = jQuery('nav');

var requestAnimationFrame = window.requestAnimationFrame
  || window.mozRequestAnimationFrame
  || window.webkitRequestAnimationFrame
  || window.msRequestAnimationFrame
  || window.oRequestAnimationFrame
  || function(update) { setTimeout(function() { update(); }, 1); };

sections.each(function() {
  $(this).data('content-height', $(this).height() + (Array.prototype.indexOf ? 0 : 280));
});

var resize_panels = function() {
  sections.css('min-height', window.innerHeight);
  // sections.each(function() {
  //   $(this).css('height', Math.max($(window).height(), $(this).data('content-height')));
  // })
};

// Internet Explorer 8 is horrible
var in_array = function(needle, haystack) {
  if (Array.prototype.indexOf)
    return haystack.indexOf(needle) != -1;
  else {
    for (var i = 0; i < haystack.length; ++i)
      if (haystack[i] == needle)
        return true;
    return false;
  }
};

// Make sure the panels fit the window
resize_panels();
jQuery(window).on('resize', resize_panels);

// When scrolling, update the parallax effect
jQuery(window).on('scroll', function() {
  sections.each(function() {
    // Only update the visible panels
    var section = this;
    if (window.scrollY + window.innerHeight > section.offsetTop
      && window.scrollY < section.offsetTop + section.offsetHeight)
      requestAnimationFrame(function() {
        jQuery(section).css('background-position', '50% ' + (window.scrollY - section.offsetTop) / 2 + 'px');
      });
  });

  // When scrolling past the navbar, make it fixed (Silly Internet Explorer, thanks MDN)
  var y = (window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;
  nav.toggleClass('fixed', y > $(window).height() - 50);
});

// Smooth-scroll internal links
jQuery('a[href^=#]')
  .filter(function() { return this.hash.length; })
  .click(function(e) {
    e.preventDefault();
    $('html,body').animate({
      scrollTop: $(this.hash).offset().top
    }, 500);
  });

// Load the other images as well
jQuery(window).load(function() {
  // Internet Explorer 8-style
  if (document.createStyleSheet)
    document.createStyleSheet('../css/images.css');
  else
    jQuery('<link>').attr({
      rel: 'stylesheet',
      href: '../css/images.css'
    }).appendTo('head');
});

// Code for initialising the Google Map with locations from the articles
// Defer this loading as we want the other images to load first
jQuery(window).load(function(){
  var $ = jQuery;
  var initialized = false;

  window.gmapscallback = function(x) {
    if (typeof google == 'undefined')
      return;

    var mapOptions = {
      disableDefaultUI: true,
      scrollwheel: false,
      navigationControl: false,
      mapTypeControl: false,
      zoomControl: true,
      zoomControlOptions: {
          style: google.maps.ZoomControlStyle.LARGE,
          position: google.maps.ControlPosition.LEFT_CENTER
      },
      scaleControl: false,
      draggable: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var canvas = $('<div class="map">').appendTo('#location');
    var map = new google.maps.Map(canvas.get(0), mapOptions);

    var bounds = new google.maps.LatLngBounds();

    $('#location .location').each(function() {
      var location = new google.maps.LatLng(
        parseFloat($(this).data('lat')),
        parseFloat($(this).data('lng')));

      bounds = bounds.extend(location);

      var infowindow = new google.maps.InfoWindow({
        content: $(this).clone().get(0)
      });

      var marker = new google.maps.Marker({
        position: location,
        map: map,
        title: $(this).find('h3').text()
      });

      infowindow.open(map, marker);

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.open(map,marker);
      });
    });

    // Add a bit of spacing to the bounds so we zoom out nicely ;)
    var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.005, bounds.getNorthEast().lng() + 0.005);
    var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.005, bounds.getNorthEast().lng() - 0.005);
    bounds.extend(extendPoint1);
    bounds.extend(extendPoint2);

    map.fitBounds(bounds);
  };

  jQuery(function($) {
    $('<script>').attr({
      src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBbHl8IIPT2U2-wqEIzgW9Y0ttFcHSd61M&sensor=false&async=2&callback=gmapscallback'
    }).appendTo('body');
  });
});

// Code for the sign up form controls
jQuery(function($) {
  var updateStudyFieldVisibility = function() {
    var relation = $('#sign-up-function').val();
    var visible = in_array(relation, ['student', 'alumnus']);
    $('#sign-up-study-row').toggle(visible);
  };

  $('#sign-up-function').on('change', updateStudyFieldVisibility);

  $('#sign-up-day-2-toggle').on('change', function() {
    $(this).closest('label').next('ul').find('input').prop('checked', this.checked);
  });

  updateStudyFieldVisibility();
});

// Code for the mobile menu
jQuery(function($) {
  var toggle = $('<li>')
    .html('&equiv;')
    .addClass('toggle')
    .click(function() {
      $('nav ul').toggleClass('open');
    });

  $('nav ul').append(toggle);

  $('nav ul li a').click(function() {
    $('nav ul').removeClass('open');
  })
});