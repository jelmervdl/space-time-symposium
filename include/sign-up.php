<?php

function check_field($name)
{
	if($_SERVER['REQUEST_METHOD'] != 'POST')
		return true;

	switch ($name)
	{
		case 'name':
			return !empty($_POST['name']) && strlen(trim($_POST['name'])) > 0;

		case 'function':
			$functions = array('student', 'alumnus', 'employee', 'former-employee', 'other');
			return !empty($_POST['function']) && in_array($_POST['function'], $functions);

		case 'study':
			// not mandatory
			return true;

		case 'email':
			return !empty($_POST['email']) && preg_match('/^.+@.+\.[a-z]+$/', $_POST['email']);

		case 'attending':
			$options = array('day-1', 'day-2-program', 'day-2-dinner', 'day-2-party');
			foreach ($options as $option)
				if (!empty($_POST[$option]))
					return true;
			return false;

		case 'remarks':
			return true;
	}
}

function escape($data)
{
	return htmlentities($data, ENT_COMPAT, 'utf-8');
}

function run_template($file, array $variables)
{
	ob_start();
	extract($variables);
	include $file;
	return ob_get_clean();
}

function handle_sign_up_form()
{
	$fields = array('name', 'function', 'study', 'email', 'attending', 'remarks');
	foreach ($fields as $field)
		if (!check_field($field))
			return false;

	// Remove attending from the fields since it is not a real form field
	$fields = array_diff($fields, array('attending'));

	// Prepare data for file
	$data = array();
	foreach ($fields as $field)
		$data[$field] = $_POST[$field];

	// Expand the attending option
	$options = array('day-1', 'day-2-program', 'day-2-dinner', 'day-2-party');
	foreach ($options as $option)
		$data['attending_' . str_replace('-', '_', $option)] = !empty($_POST[$option]) ? $option : '';

	// Store data in the text file
	$fhandle = fopen('../var/signups.csv', 'a');
	fputcsv($fhandle, $data);
	fclose($fhandle);

	// Email the data
	$headers = array(
		'MIME-Version: 1.0',
		'Content-type: text/html; charset=utf-8',
		sprintf('To: %s <%s>', $data['name'], $data['email']),
		'From: lustrumki@gmail.com',
		'Bcc: lustrumki@gmail.com');

	$content = run_template('email.php', $data);

	preg_match('{<title>(.+?)</title>}', $content, $subject);

	mail(sprintf('%s <%s>', $data['name'], $data['email']),
		$subject[1], $content, implode("\r\n", $headers));

	// Submission successful? Clear data from form (ugly hack? Yeah, pretty ugly)
	$_POST = array();
	$_SERVER['REQUEST_METHOD'] = 'GET';

	return true;
}

function begin_form_row($name)
{
	$attributes = '';

	if (!check_field($name))
		$attributes .= ' class="error"';

	printf("<div id=\"sign-up-%s-row\"%s>\n", $name, $attributes);
}

function end_form_row()
{
	printf("</div>\n");
}

function form_escape($value)
{
	return htmlspecialchars($value, ENT_COMPAT, 'UTF-8');
}

function form_attributes($attributes)
{
	$attribute_html = array();

	foreach ($attributes as $key => $value)
		if (is_int($key))
			$attribute_html[] = $value;
		else
			$attribute_html[] = sprintf('%s="%s"', $key, form_escape($value));

	return implode(' ', $attribute_html);
}

function form_field($type, $name, $attributes = array())
{
	$attributes['type'] = $type;
	$attributes['name'] = $name;
	$attributes['id'] = 'sign-up-' . $name;

	if (isset($_POST[$name]))
		$attributes['value'] = $_POST[$name];

	printf("<input %s>\n", form_attributes($attributes));
}

function form_select($name, array $attributes, array $options)
{
	$attributes['name'] = $name;
	$attributes['id'] = 'sign-up-' . $name;
	
	$options_html = array();

	foreach ($options as $value => $option)
	{
		$option_attributes = !empty($option[1]) ? $option[1] : array();

		if (!is_int($value))
			$option_attributes['value'] = $value;

		if (isset($_POST[$name]) && $_POST[$name] == $value)
			$option_attributes[] = 'selected';

		$options_html[] = sprintf("\t<option %s>%s</option>",
			form_attributes($option_attributes),
			form_escape($option[0]));
	}

	printf("<select %s>\n%s</select>\n",
		form_attributes($attributes),
		implode("\n", $options_html));
}

function form_textarea($name, array $attributes)
{
	$attributes['name'] = $name;
	$attributes['id'] = 'sign-up-' . $name;

	$value = isset($_POST[$name]) ? $_POST[$name] : '';

	printf("<textarea %s>%s</textarea>\n",
		form_attributes($attributes),
		form_escape($value));
}

function form_checkbox($name, array $attributes = array())
{
	$attributes['type'] = 'checkbox';
	$attributes['name'] = $name;
	$attributes['id'] = 'sign-up-' . $name;

	if (!empty($_POST[$name]))
		$attributes[] = 'checked';

	printf("<input %s>", form_attributes($attributes));
}

include 'sign-up-form.php';

?>